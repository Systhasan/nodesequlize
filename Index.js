// init express
const express = require('express')

// init morgan
const logger = require('morgan')

// init body-parser
const bodyParser = require('body-parser')

//init http
const http = require('http')

// set express app
const app = express()

// Log requrests to the console.
app.use(logger('dev'))

// Parse incomming requests data
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:false}));

// Models
var models = require("./models");
// Sync DB
models.sequelize.sync().then(function(){
    console.log('Nice, DB looks fine..!');
}).catch(function(err){
    console.log(err, "Something went wrong with the Database Update!");
});

require('./routes')(app);

// Setup a default catch-all route that sends back a welcome message in json format.
app.get('*', (req, res)=> res.status(200).send({message: 'Welcome to the beginning of nothingness.'}));

// init request port
const port = parseInt(process.env.PORT, 10) || 8080
app.set('port', port)

// create server
const server = http.createServer(app)

// run server on assign port. 
server.listen(port, ()=>console.log(`Server start on port no ${port}`))

// export app object.
module.exports = app;