'use strict';
module.exports = (sequelize, DataTypes) => {
  const Employee = sequelize.define('Employee', {
    name: { 
      type: DataTypes.STRING,
      allowNull: false        
    },
    designation: {
      type:DataTypes.STRING,
      allowNull:false
    },
    salary: {
      type: DataTypes.NUMBER,
      allowNull:false
    }
  }, {});
  Employee.associate = function(models) {
    // associations can be defined here
    Employee.belongsTo(models.Company, {
      foreignKey: 'companyId',
      onDelete: 'CASCADE'
    });

    Employee.hasOne(models.Profile, {
      foreignKey: 'user_id',
      as: 'user_id'
    });
  };
  return Employee;
};