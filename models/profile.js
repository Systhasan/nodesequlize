'use strict';
module.exports = (sequelize, DataTypes) => {
  const Profile = sequelize.define('Profile', {
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING,
    user_id: DataTypes.INTEGER
  }, {});
  Profile.associate = function(models) {
    // associations can be defined here
    Profile.belongsTo(models.Employee, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE'
    });
  };
  return Profile;
};