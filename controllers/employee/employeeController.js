/** 
 * @type Controller
 * @name employeeController
 * @desc this file will be work on employee database. 
 *       any kind of employee data will be handle here. 
 * @created_by  Muhammad Hasan
 * @created_at 06th August 2019
*/


/** 
 * @type load & Init
 * @name Company
 * @desc init Company model
*/
const Company = require('../../models').Company;

/** 
 * @type load & Init
 * @name Employee
 * @desc init employee model
*/
const Employee = require('../../models').Employee;

// Status Code
// 200 Status OK
// 201 Status Created


// export employee functions
module.exports = {
    
    /** 
     * @name create
     * @type method
     * @params {object} req, {object} res
     * @desc add new employee data to the database.
    */
    create(req, res){
        return Employee.create({
            name:req.body.name,
            designation:req.body.designation,
            salary:req.body.salary,
            companyId:req.body.company_id
        }).then((result) => res.status(201).send(result))
        .catch(err => res.status(400).send(err))
    },

    /** 
     * @name findEmployee
     * @type method
     * @params {object} req, {object} res
     * @return object
     * @desc find en employee by using primary key (ID).
    */
    findEmployee(req, res){
        return Employee.findByPk(req.body.id)
        .then(emp => res.status(200).send(emp))
        .catch(err => res.status(404).send(err))
    },


    /** 
     * @name getAllEmployee
     * @type method
     * @params {object} req, {object} res
     * @return object
     * @desc get all employee data.
    */
    getAllEmployee(req, res){
        return Employee
        .findAll({
            include: [{
                model: Company
            }],
            attributes: {
                exclude: ['salary']
            }
        })
        .then( (result) => {
          res.status(200).send(result)
        }).catch(err => res.status(400).send(err));
    },


    /** 
     * @name updateEmployee
     * @type method
     * @params {object} req, {object} res
     * @desc update an employee data using given employee id. 
    */
    updateEmployee(req, res){
        return Employee.update(
        {
            name:req.body.name,
            designation:req.body.designation,
            salary:req.body.salary,
            companyId:req.body.companyId,
        },
        {
            where: {
                id: req.body.id
            }
        }).then( emp => res.status(200).send(emp))
        .catch(err => res.status(400).send(err))
    },


    /** 
     * @name deleteEmployee
     * @type method
     * @params {object} req, {object} res
     * @desc delete an employee using given employee id.
    */
    deleteEmployee(req, res){
        return Employee.destroy({
            where: {
                id: req.body.id
            }
        }).then( (result) => {
            res.status(200).send(result)
        })
        .catch( (err) => res.status(400).send(err))
    },

    findEmployeeByRowData(req, res){
        var column_name = String(req.body.columnName)
        console.log( {column_name:4} )
        console.log(req.body.columnName)
        res.status(200).send.json('D')
        return Employee.findAll({
            where:{
                companyId:4,
                id:6
            }
        }).then( (result) => res.status(200).send(result))
        .catch( (err) => res.status(400).send(err))
    }
}