const company = require('./company/companyController');
const employee = require('./employee/employeeController');

// init our controllers object as an object.
module.exports = {
    company,
    employee
}