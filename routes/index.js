const companyController = require('../controllers').company;
const employeeController = require('../controllers').employee;

module.exports = (app) => {
    app.post('/api/add_company', companyController.create);

    // Company
    

    // Employee
    app.post('/api/all_employee', employeeController.getAllEmployee)
    app.post('/api/add_employee', employeeController.create);
    app.post('/api/update_employee', employeeController.updateEmployee);
    app.post('/api/get_employee', employeeController.findEmployee);
    app.post('/api/delete_employee', employeeController.deleteEmployee);
    app.post('/api/get_employee_by_value', employeeController.findEmployeeByRowData);
}